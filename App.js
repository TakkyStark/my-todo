import React, { Component } from 'react';
import List from './components/list'

class App extends Component {
  state = {
    input: '',
    lists: [],
  }

  updateInput(e) {
    const value = e.target.value
    this.setState({input: value})
  }

  handleSubmit() {
    console.log('you submit', this.state.input);
    this.setState({input: '', lists: [...this.state.lists, this.state.input]})
  }

  handleDel(e) {
    this.state.lists.splice(e, 1)
    this.setState({lists: this.state.lists})
  }

  showUsers() {
    fetch('https://jsonplaceholder.typicode.com/users')
  .then(response => response.json())
  .then(json => {
    const name = json.map((user, i) => {
      return user.name
    })
    this.setState({lists: [...this.state.lists, ...name]})
    console.log(json);
  })
}

handleInfo(e) {
  this.state.lists.splice(e+1, 0, 'info of: '+ e)
  this.setState({lists: [...this.state.lists, e]})
}

reset() {
  this.setState({lists: []})
}

  render() {
    return (
      <div>
      <input value={this.state.input} onChange={this.updateInput.bind(this)} type= "text" placeholder = "Enter your task ..." />
      <button onClick={this.handleSubmit.bind(this)} type = "Submit">Submit</button>
      <button onClick={this.showUsers.bind(this)} type="Button">generate users</button>
      <button onClick={this.reset.bind(this)} type="Button">reset</button>
      {this.state.lists.map((list, i) => {
        return <List key={i} id={i} text={list} del={this.handleDel.bind(this)} info={this.handleInfo.bind(this)}/>
      })}
      </div>
    )
  }
}

export default App;
