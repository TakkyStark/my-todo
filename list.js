import React from 'react'

class List extends React.Component {
  handleDel() {
    this.props.del(this.props.id)
  }

  showInfo() {
    this.props.info(this.props.id)
  }

  render() {
    return(
      <div>
      <li>{this.props.text}
      <button onClick={this.showInfo.bind(this)}>info</button>
      <button onClick={this.handleDel.bind(this)}>del</button>
      </li>
      </div>
    )
  }
}

export default List
